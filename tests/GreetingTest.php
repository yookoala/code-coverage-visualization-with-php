<?php

declare(strict_types=1);

use Koala\CodeburtaPhpTest\Greeting;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Koala\CodeburtaPhpTest\Greeting
 */
final class GreetingTest extends TestCase
{
    public function testGetMessage(): void
    {
        $g = new Greeting('Hello, @name!');
        $this->assertEquals(
            'Hello, @name!',
            $g->getMessage(),
            'Should be able to get the inner message correctly',
        );
    }

    public function testSayTo(): void
    {
        $g = new Greeting('Hello, @name!');
        $this->assertEquals(
            'Hello, Tommy!',
            $g->sayTo('Tommy'),
            'Should be able to get the inner message correctly',
        );
    }
}

